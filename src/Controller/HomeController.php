<?php

namespace App\Controller;

use App\Service\Unsplash;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends Controller
{
    /**
     * Home page
     * @Route("/", name="home")
     * @return Response
     */
    public function index()
    {

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
     * Proccess AJAX request to search for photos
     *
     * @param Request $request
     * @Route( "/search", name="search", methods={"POST"})
     * @return Response
     */
    public function search(Request $request)
    {
        $unsplash = new Unsplash();
        $query = $request->request->get("search");

        $page = $request->request->has("page") ? $request->request->get("page") : 1;

        if(!empty($query)){
            $photos = $unsplash->searchPhotos($query, $page);

            if($photos !== false && $photos->total > 0){
                return $this->render("home/search.html.twig", [
                    'photos' => $photos,
                    'page' => $page
                ]);
            }

        }

        return $this->render("home/nosearch.html.twig", []);


    }
}
