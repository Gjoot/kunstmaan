<?php


namespace App\Service;
use Curl\Curl;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;


/**
 * Class Unsplash
 * @package App\Service
 * Service to get photos from unsplash api
 * https://unsplash.com/documentation
 */
class Unsplash
{

    private $unsplashApiUrl = "https://api.unsplash.com/";
    //only cache for 15 min so that request still can updates from unsplash
    private $cacheExpires = 900;

    /**
     * Get photo's from unsplash
     * @param string $search
     * @param int $page
     * @return object | false
     */
    public function searchPhotos(string $search, int $page = 1)
    {

        if(!empty($search)){
            return $this->getPhotos($search, $page);
        }

        return false;
    }

    /**
     * Get photo's from unsplash cached or make request
     * @param $search
     * @param int $page
     * @return object | false
     */
    private function getPhotos(string $search, int $page = 1)
    {

        $cache = new FilesystemAdapter("app.unsplash");

        // create a unique key based on hashed
        $cacheKey = sha1($search) . "_" . $page;

        $cacheItem = $cache->getItem($cacheKey);

        //check if we cached it already, if so return it
        if($cacheItem->isHit()){
            return $cacheItem->get();
        }else{
            //there is no cache so we make a request to unsplash
            $photos = $this->requestPhotos($search, $page);

            // if the request is not false we cache it
            if ($photos !== false){
                $cacheItem->set($photos);
                $cacheItem->expiresAfter($this->cacheExpires);
                $cache->save($cacheItem);
            }

            return $photos;
        }

    }

    /**
     * Make curl request to unsplashed
     * @param $query
     * @param int $page
     * @return object|false
     */
    private function requestPhotos(string $query, int $page = 1)
    {

        $curl = new Curl();
        $curl->setHeader('Authorization', 'Client-ID ' . getenv("UNSPLASH_KEY"));
        $curl->get($this->unsplashApiUrl . "search/photos",
            array(
                'query' => $query,
                'page' => $page
            )
        );

        if ($curl->error) {
            return false;
        }
        else {
            return json_decode($curl->response);
        }

    }

}